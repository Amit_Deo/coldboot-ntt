"""
An (inefficient) set of functions that compute minimal BSDR lists
"""

from sage.all import vector, cached_function, ZZ


def bsdr_from_vec(v, bits):
    """
    Returns a list of all minimal BSDR representations of a vector v

    :param v: input vector
    :param bits: number of bits in BSDR for each component of v

    EXAMPLE::

        sage: load("bsdr.py")
        sage: set_random_seed(0)
        sage: v = vector([randint(-15,15) for _ in range(4)])
        sage: v
        (-12, 0, -14, -5)
        sage: bsdr_from_vec(v,5)
        [(0, 0, 1, 0, -1, 0, 0, 0, 0, 0, 0, 1, 0, 0, -1, -1, 0, -1, 0, 0),
        (0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, -1, -1, 0, -1, 0, 0)]

    """
    components_list = []
    for vi in v:
        components_list.append(bsdr_from_int(vi, bits))
    return create_vecs_from_components(components_list)


def create_vecs_from_components(components_list):
    """
    The ith list in components_list should be the the list of possibilities of the ith component of
    the original vector.
    """
    n = len(components_list)
    current_list = components_list[0]
    for i in range(1, n):
        next_list = add_prefix_to_suffices(current_list, components_list[i])
        current_list = next_list
    return map(vector,  current_list)


@cached_function
def bsdr_from_int(s, bits):
    """
    Return a list of minimal BSDRs of an integer with some maximal number of bits

    :param s: an integer
    :param bits: the maximum number of bits in the output

    EXAMPLE::

        sage: load("bsdr.py")
        sage: bsdr_from_int(31,6)
        [[-1, 0, 0, 0, 0, 1]]

    """
    if bits == 0:
        return [[]]
    if s == 0 or s % 2**(bits) ==0:
        return [[0]*bits]

    for i in range(bits):
        if s % 2**(i+1) != 0:
            break

    # try two lists, one for -1 and one for +1
    prefix1 = [0]*i + [1]
    prefix2 = [0]*i + [-1]

    suffixes1 = bsdr_from_int((s-2**i)//(2**(i+1)), bits-(i+1))
    suffixes2 = bsdr_from_int((s+2**i)//(2**(i+1)), bits-(i+1))

    l1 = add_prefix_to_suffices(prefix1, suffixes1)
    l2 = add_prefix_to_suffices(prefix2, suffixes2)

    entire_list = l1+l2
    purge_list(s, entire_list)

    min_list = minimal_hw_list(entire_list)
    return min_list


def purge_list(target, ll):
    """
    Purge a list of candidates ll of all wrong BSDRs

    :param target: the integer we want to express in BSDR
    :param ll: list of candidate BSDRs
    """
    for l in ll:
        if ZZ(l, base=2) != target:
            ll.remove(l)


def add_prefix_to_suffices(prefixes, suffixes):
    """
    Combine a list of prefixes with a list of suffixes.

    :param prefixes: list of prefixes
    :param suffixes: list of suffixes

    EXAMPLE::

        sage: load("bsdr.py")
        sage: prefixes = [[1,2,3],[4,5,6]]; suffixes = [[10,20,30],[40,50,60]]
        sage: add_prefix_to_suffices(prefixes,suffixes)
        [[1, 2, 3, 10, 20, 30],
         [1, 2, 3, 40, 50, 60],
         [4, 5, 6, 10, 20, 30],
         [4, 5, 6, 40, 50, 60]]
    """
    # prefixes is a list of prefixes
    if type(prefixes[0]) == list:
        output = []
        for i in prefixes:
            for j in suffixes:
                output.append(i+j)
        return output
    else:  # then prefixes is a single prefix
        output = []
        for i in suffixes:
            output.append(prefixes+i)
        return output


def minimal_hw_list(inlist):
    """
    Return the minimal hamming weight entries of the input list

    :param inlist: a list of bit string lists of the same length

    """
    outlist = []
    min_hw = len(inlist[0])+1   # gets reset in the first loop
    for l in inlist:
        hw = vector(l).hamming_weight()
        if hw == min_hw:
            outlist.append(l)
        elif hw < min_hw:
            del(outlist)
            outlist = []
            outlist.append(l)
            min_hw = hw

    return outlist
