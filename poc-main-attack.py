# -*- coding: utf-8 -*-
"""
Proof of concept implementation of cold boot attacks on R/MLWE under the NTT

We consider the following scenario. Let `s` be some vector of length `n` with small elements mod
`q`.  We are given `\hat{s} = NTT*s + Δ` (in the code below, we use ``d`` to denote `Δ` to highlight
it is a vector, not a matrix) and want to recover `s`.  We write: `c = NTT^{-1}\hat{s} = s + iNTT
⋅ Δ`.

..  note :: Run tests by ``sage -t poc-main-attack.py``

EXAMPLE::

    sage: load("poc-main-attack.py")
    sage: ip = InstanceParams(n=256, q=7681, eta=4, kappa=19)
    sage: pp = PreprocParams(q=7681, fold=3, alpha=5, beta=2)

"""
import sys
from sage.all import ceil, log, vector, ZZ, shuffle, GF, RR, QQ, sqrt
from sage.all import matrix, set_random_seed, show, load, line
from sage.all import parent, binomial, cached_function, median
from fpylll import BKZ
from collections import namedtuple

# HACK
sys.path.append(".")
from bsdr import bsdr_from_int  # noqa


"Utility functions"


def basis_shape(A, showit=True):
    """
    A's geometry

    :param A: an integer matrix
    :param showit: show a plot of the lengths of the Gram-Schmidt vectors

    """
    from fpylll import IntegerMatrix, GSO
    A = IntegerMatrix.from_matrix(A)
    M = GSO.Mat(A)
    M.update_gso()
    r = M.r()
    mmin = min(r)
    mmax = max(r)
    print "min: (%3d, %6.1f)"%(r.index(mmin), mmin),
    print "max: (%3d, %6.1f)"%(r.index(mmax), mmax),
    print
    if showit:
        show(line(zip(range(M.d), map(lambda x: log(x, 2), M.r()))))


def balance(e, q=None):
    """
    Return a balanced representation between `-q/2` and `q/2` for `e`.

    :param e: a scalar, polynomial or vector
    :param q: optional modulus

    """
    try:
        p = parent(e).change_ring(ZZ)
        return p([balance(e_, q=q) for e_ in e])
    except (TypeError, AttributeError):
        if q is None:
            try:
                q = parent(e).order()
            except AttributeError:
                q = parent(e).base_ring().order()
        return ZZ(e)-q if ZZ(e)>q/2 else ZZ(e)


def snort(s, q, ell=None, b=None):
    """
    Return the vector `s` written in base `2^ℓ` scaled by factor `θ`

    :param s: a vector (over the integers)
    :param q: an integer modulus
    :param ell: the vector will be rewritten in base `2^ℓ`
    :param b: if not ``None`` only consider the lower `2^{bℓ}` bits.
              This assume the values of `s` are bounded by `2^{bℓ}`

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: d = bitflipf(n=16, q=7681, kappa=10)
        sage: snort(d, 7681)
        (-1, 32, 0, 32, 0, 0, 4, 0, 0, -32, 0, 0, 0, 0, -32, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 1, -4)

    """
    ell = PreprocParams.ellf(q, ell)
    v = []
    b = PreprocParams.bf(q, ell, b)
    for s_ in s:
        s_ = ZZ(s_)
        s_ = bsdr_from_int(s_, ell*b)[0]
        for i in range(0, ell*b, ell):
            v.append(ZZ(list(s_[i:i+ell]), base=2))
    return vector(v)


def sneeze(v, q, ell=None, theta=1, b=None):
    """Inverse of snort

    :param v: vector holding `2^ℓ` decomposition of a vector `s`
    :param q: an integer modulus
    :param ell: the vector will be rewritten in base `2^ℓ`
    :param theta: the rewritten vector will be scaled by `θ in QQ`
    :param b: if not ``None`` only consider the lower ``2^{bℓ}` bits

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: d = bitflipf(n=16, q=7681, kappa=10)
        sage: d_ = snort(d, 7681)
        sage: d == sneeze(d_, 7681)
        True

    """
    s = []
    ell = int(PreprocParams.ellf(q, ell))
    b = int(PreprocParams.bf(q, ell, b))
    base = 2**ell
    v = list(v)
    if theta == 1:
        s = vector([ZZ(v[i:i+b], base=base) for i in range(0, len(v), b)])
    else:
        s = vector([ZZ(v[i:i+b], base=base)//theta for i in range(0, len(v), b)])
    return s


"""Our instances are characterised by `n, q, σ, κ` where the first three are LWE-like parameters and
the last one denotes the hamming weight of `Δ` considered as a bitstring.
"""


class InstanceParams(namedtuple("InstanceParams", ["n", "eta", "q", "kappa"])):
    @property
    def sd(self):
        return sqrt(self.eta)


@cached_function
def intt_matrix(n, q, omega=None, gamma=None):
    """
    Inverse NTT matrix.

    :param n: dimension
    :param q: modulus
    :param omega: enforce a choice for omega
    :param gamma: enforce a choice for gamma

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: W = intt_matrix(8, 7681)
        sage: W*vector(ZZ, 8*[1]) ## constant polynomial evaluated at anything is `1`
        (1, 0, 0, 0, 0, 0, 0, 0)

    """

    K = GF(q)
    if omega is None and gamma is None:
        om = K(1).nth_root(n)
        gam = om.nth_root(2)
    else:
        om = K(omega)
        gam = K(gamma)
    inv_n = K(n)**(-1)
    W = matrix(K, n, n)
    for i in range(n):
        for j in range(n):
            W[i, j] = inv_n * gam**(-i) * om**(-i*j)
    return W


@cached_function
def ntt_matrix(n, q, omega=None, gamma=None):
    K = GF(q)
    if omega is None and gamma is None:
        om = K(1).nth_root(n)
        gam = om.nth_root(2)
    else:
        om = K(omega)
        gam = K(gamma)
    W = matrix(K, n, n)
    for i in range(n):
        for j in range(n):
            W[i, j] = gam**(j) * om**(i*j)
    return W


def bitflipf(n, q, kappa, s_hat=None):
    """
    Sample a secret vector `Δ` of length `n` mod `q` with `κ` bits set in random positions.

    :param n: dimension
    :param q: an integer modulus
    :param k: number of non-zero bits
    :param s_hat: clean NTT(s) to derive the bit flip signs from

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: d = bitflipf(16, 7681, 10); d
        (4095, 4096, 0, 4, -4096, 0, 0, -544, 0, 0, 0, 0, 4096, 0, 0, -511)

        sage: bitstring = sum(map(lambda x: bsdr_from_int(x, 13), d), [])
        sage: # len(bitstring)-bitstring.count(0)
        10

    """
    ell = ceil(log(q, 2))
    positions = [1]*kappa + [0]*(n*ell-kappa)
    shuffle(positions)

    d = []
    if s_hat is None:
        for i in range(n):
            d.append(sum((-1)**ZZ.random_element(0, 2) * positions[i*ell+j] * 2**j for j in range(ell)))
            # d.append((-1)**ZZ.random_element(0, 2)*sum(positions[i*ell+j]*2**j for j in range(ell)))
    else:
        for i in range(n):
            bin_s_hat = s_hat[i].digits(base=2, padto=ell)
            d.append(sum(((-1)**(bin_s_hat[j]))*positions[i*ell+j]*2**j for j in range(ell)))

    return vector(ZZ, d)


def instance(params, seed=None):
    """
    Generate a new LWE-like challenge for coldboot attacks, `c = W⋅d + s`, with `s` small and `d`
    coming from cold boot bit flips.

    :param params: instance parameters
    :param seed: a random seed (or ``None``)

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: ip = InstanceParams(n=16, q=7681, eta=4, kappa=10)
        sage: instance(ip, seed=0) == instance(ip, seed=0)
        True

        sage: (W,c), (d,s) = instance(ip, seed=1)
        sage: c == W*d + s
        True

    """
    if seed is not None:
        set_random_seed(seed)

    if params.q == 7681 and params.n <= 256:
        # page 4 of Kyber spec
        gamma = GF(params.q)(62)
        omega = GF(params.q)(3844)
        n_ = params.n
        while n_ < 256:
            gamma = gamma**2
            omega = omega**2
            n_ = 2*n_
        assert(omega**(params.n) == 1)
        assert(gamma**(2*params.n) == 1)
        assert(omega**(params.n//2) == -1)
        assert(gamma**(params.n) == -1)
    elif params.q == 12289 and params.n <= 1024:
        # newhope spec
        gamma = GF(params.q)(7)
        omega = GF(params.q)(49)
        n_ = params.n
        while n_ < 1024:
            gamma = gamma**2
            omega = omega**2
            n_ = 2*n_
        assert(omega**(params.n) == 1)
        assert(gamma**(2*params.n) == 1)
        assert(omega**(params.n//2) == -1)
        assert(gamma**(params.n) == -1)
    else:
        omega, gamma = None, None

    W = intt_matrix(params.n, params.q, omega=omega, gamma=gamma)
    inv_W = ntt_matrix(params.n, params.q, omega=omega, gamma=gamma)

    D = lambda eta: sum(-ZZ.random_element(0, 2) + ZZ.random_element(0, 2) for _ in range(eta))  # noqa
    s = vector(ZZ, [D(params.eta) for i in range(params.n)])
    s_hat = inv_W * s
    d = bitflipf(params.n, params.q, params.kappa, vector(ZZ, s_hat))
    c = W * d + s
    return (W, c), (d, s)


"Preprocessing handles the combinatorial aspect of the attack."


class PreprocParams(object):
    def __init__(self, q=7681, ell=None, alpha=0, beta=1, b=None, shave_first=False, fold=3, even=True):
        """

        :param q: Integer modulus, used to derive `ℓ` if that is not provided
        :param ell: We write elements in base `2^ℓ`
        :param alpha: We guess α bits in the `β` top-most bits
        :param beta: We guess α bits in the `β` top-most bits
        :param b: The highest power of 2^ℓ we consider is (b-1)
        :param shave_first: shave before or after folding (after seems better)
        :param fold: number of folding levels

        """
        self.ell = PreprocParams.ellf(q, ell=ell)
        self.q = q
        self.b = PreprocParams.bf(q, ell=ell, b=b)
        self.alpha = alpha
        self.beta = beta
        self.shave_first = shave_first
        self.fold = fold
        self.even = even

    @staticmethod
    def ellf(q, ell=None):
        """
        By default `2^ℓ ≈ sqrt(q)`
        """
        if ell is None:
            ell = ceil(log(q, 2.)/2.)
        return ell

    @staticmethod
    def bf(q, ell=None, b=None):
        """Return the length of writing `q` in base `2^ℓ`

        :param q: an integer modulus
        :param ell: the vector will be rewritten in base `2^ℓ`
        :param b: if not ``None`` this value is simply returned, i.e. this function computes nothing.

        """
        ell = PreprocParams.ellf(q, ell)

        if b is not None:
            return b
        else:
            return ceil(log(q, 2)/ell)


def shave(v, params):
    """
    Remove the α most significant bits from `v`. This is as though successfully guessing the α
    biggest parts of `v` lying in a band of the top β bits.

    .. note :: If ``even`` is set in params, this function first ensures that Δ_i % 2 ≡ 0

    :param v: a vector over the integers
    :param params: preprocessing parameters

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: d = bitflipf(n=16, q=7681, kappa=10); d
        (4095, 4096, 0, 4, -4096, 0, 0, -544, 0, 0, 0, 0, 4096, 0, 0, -511)
        sage: snort(d, 7681)
        (-1, 32, 0, 32, 0, 0, 4, 0, 0, -32, 0, 0, 0, 0, -32, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 1, -4)

        sage: pp = PreprocParams(q=7681, alpha=2, beta=2)
        sage: d_, _ = shave(d, pp); d_
        (4096, 4096, 0, 4, -4096, 0, 0, -544, 0, 0, 0, 0, 4096, 0, 0, -512)

        sage: snort(d_, 7681)
        (0, 32, 0, 32, 0, 0, 4, 0, 0, -32, 0, 0, 0, 0, -32, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 0, -4)

        sage: snort(d, 7681) - snort(d_, 7681)
        (-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0)

        sage: d_, o_ = shave(d, pp)
        sage: d == d_ + o_
        True

    We contrast the behaviour above with that of ``even==False``

        sage: pp = PreprocParams(q=7681, alpha=2, beta=2, even=False)
        sage: d_, _ = shave(d, pp); d_
        (-1, 0, 0, 4, -4096, 0, 0, -544, 0, 0, 0, 0, 4096, 0, 0, -511)

        sage: snort(d_, 7681)
        (-1, 0, 0, 0, 0, 0, 4, 0, 0, -32, 0, 0, 0, 0, -32, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 1, -4)

        sage: snort(d, 7681) - snort(d_, 7681)
        (0, 32, 0, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

        sage: d_, o_ = shave(d, pp)
        sage: d == d_ + o_
        True

    """
    v = snort(v, params.q, ell=params.ell, b=params.b)
    d = vector(ZZ, len(v))

    fixed = 0

    if params.even:
        for i in range(params.alpha):
            for j, s_ in enumerate(v):
                if s_ % 2 and j % 2 == 0:
                    break
            else:
                break

            k = 0
            fixed += 1

            if v[j] > 0:
                d[j] += 2**k
                v[j] -= 2**k
            else:
                d[j] -= 2**k
                v[j] += 2**k

    for i in range(params.alpha-fixed):
        vv = map(lambda x: bsdr_from_int(x, params.ell)[0], v)

        j, k = 0, 0
        for j_ in range(len(vv)):
            for k_ in range(k, params.ell)[::-1]:
                if vv[j_][k_] and k_ > k:
                    j, k = j_, k_

        if k < params.ell-params.beta:
            break

        d[j] += vv[j][k]*2**k
        v[j] -= vv[j][k]*2**k

    s = sneeze(v, params.q, ell=params.ell, b=params.b)
    d = sneeze(d, params.q, ell=params.ell, b=params.b)
    return s, d


def shave_cost(ip, pp):
    """
    The cost of shaving.

    :param ip: instance parameters
    :param pp: preprocessing parameters

    EXAMPLE:

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=256, q=7681, eta=4, kappa=19)
        sage: ceil(shave_cost(ip, PreprocParams(q=7681, alpha=5, beta=2, fold=3)).log(2).n())
        34

        sage: ceil(shave_cost(ip, PreprocParams(q=7681, alpha=5, beta=2, fold=2)).log(2).n())
        39

        sage: ceil(shave_cost(ip, PreprocParams(q=7681, alpha=10, beta=2, fold=2)).log(2).n())
        68

        sage: ceil(shave_cost(ip, PreprocParams(q=7681, alpha=5, beta=3, fold=2)).log(2).n())
        41

        sage: ceil(shave_cost(ip, PreprocParams(q=previous_prime(2^14), alpha=5, beta=2, fold=2)).log(2).n())
        40

    """

    lq = ceil(log(pp.q, 2))
    if pp.shave_first:
        n = ip.n
        fct = 1
    else:
        n = ip.n//2**pp.fold
        fct = 2

    if lq == pp.b*pp.ell:
        nbits = n * (pp.b * pp.beta)
    else:
        diff = pp.b*pp.ell - lq
        nbits = n * (pp.b-1) * pp.beta + n * (pp.beta-diff)

    nbits = max(nbits, 0)  # for beta = 0 the above can be negative
    if pp.even:
        nbits += n  # We need to handle the n lowest order bits
    # We get an additional factor of 2^i because we also need to guess signs
    return sum(fct**i * binomial(nbits, i) for i in range(pp.alpha+1))


def preprocess(pk, sk, params):
    """

    Apply all preprocessing (folding & shaving)

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=16, q=7681, eta=4, kappa=10)
        sage: (W,c), (d,s) = instance(ip, seed=0)

        sage: (W_, c_), (d_, s_) = preprocess((W,c), (d,s), PreprocParams(q=7681, fold=1))
        sage: c_ == W_*d_ + s_
        True

        sage: (W_, c_), (d_, s_) = preprocess((W,c), (d,s), PreprocParams(q=7681, fold=2))
        sage: c_ == W_*d_ + s_
        True

        sage: (W_, c_), (d_, s_) = preprocess((W,c), (d,s), PreprocParams(q=7681, alpha=1, beta=4))
        sage: c_ == W_*d_ + s_
        True

        sage: (W_, c_), (d_, s_) = preprocess((W,c), (d,s), PreprocParams(q=7681, alpha=1, beta=4, shave_first=True))
        sage: c_ == W_*d_ + s_
        True

    """

    W, c = pk
    d, s = sk

    assert c == W*d + s
    assert ~W*c == d + ~W*s

    q = W.base_ring().order()

    # We do all computations on the forward NTT, thus we flip everything around

    W, c = ~W, ~W*c
    d, s = s, d

    if params.shave_first and params.alpha:
        s, correction = shave(s, params=params)
        c -= correction

    assert c == W*d + s

    for _ in range(params.fold):
        n = len(c)
        W_, c_, d_, s_ = [], [], [], []
        for i in range(n/2):
            W_.append(W[i][0:n:2] + W[i+n/2][0:n:2])
            c_.append(c[i] + c[i+n/2])
            s_.append(s[i] + s[i+n/2])
            d_.append(d[2*i])
        c = vector(c_)
        W = matrix(W_)
        d = balance(vector(d_), q)
        s = vector(s_)

        assert c == W*d + s

    if not params.shave_first and params.alpha:
        s, correction = shave(s, params=params)
        c -= correction

    # We do all computations on the inverse NTT, thus we flip everything around

    W, c = ~W, ~W*c
    d, s = s, d

    return (W, c), (d, s)


def challenge(ip, pp, seed=None):
    """
    A challenge for the attack.

    :param ip: instance parameters
    :param pp: processing parameters
    :param seed: random seed

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=16, q=7681, eta=4, kappa=10)
        sage: pp = PreprocParams(q=7681, fold=1, alpha=4, beta=3)
        sage: (W,c), (d,s) = challenge(ip, pp, seed=1)
        sage: W*d + s == c
        True

    """
    pk, sk = instance(ip, seed=seed)
    pk, sk = preprocess(pk, sk, pp)
    return pk, sk


def lattice_basis((A, c), params, theta=1):
    """
    Construct a primal attack uSVP lattice.

    :param A: LWE-like matrix
    :param c: LWE-like vector
    :param params: preprocessing parameters
    :param theta: scale the columns of the secret by this factor

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=8, q=7681, eta=4, kappa=10)
        sage: pp = PreprocParams(q=7681, fold=1, alpha=4, beta=3, even=False)
        sage: (W,c), (d,s) = challenge(ip, pp, seed=1)
        sage: lattice_basis((W, c), pp, theta=1)
        [   1    0    0    0    0    0    0    0 6721 3121 1383 4649    0]
        [   0    1    0    0    0    0    0    0   16   76  361 3635    0]
        [   0    0    1    0    0    0    0    0 6721 4649 6298 3121    0]
        [   0    0    0    1    0    0    0    0   16 3635 7320   76    0]
        [   0    0    0    0    1    0    0    0 6721 4560 1383 3032    0]
        [   0    0    0    0    0    1    0    0   16 7605  361 4046    0]
        [   0    0    0    0    0    0    1    0 6721 3032 6298 4560    0]
        [   0    0    0    0    0    0    0    1   16 4046 7320 7605    0]
        [   0    0    0    0    0    0    0    0 7681    0    0    0    0]
        [   0    0    0    0    0    0    0    0    0 7681    0    0    0]
        [   0    0    0    0    0    0    0    0    0    0 7681    0    0]
        [   0    0    0    0    0    0    0    0    0    0    0 7681    0]
        [   0    0    0    0    0    0    0    0 4977 4668 5607 1754   -1]

        sage: lattice_basis((W, c), pp, theta=2)
        [    1     0     0     0     0     0     0     0 13442  6242  2766  9298     0]
        [    0     1     0     0     0     0     0     0    32   152   722  7270     0]
        [    0     0     1     0     0     0     0     0 13442  9298 12596  6242     0]
        [    0     0     0     1     0     0     0     0    32  7270 14640   152     0]
        [    0     0     0     0     1     0     0     0 13442  9120  2766  6064     0]
        [    0     0     0     0     0     1     0     0    32 15210   722  8092     0]
        [    0     0     0     0     0     0     1     0 13442  6064 12596  9120     0]
        [    0     0     0     0     0     0     0     1    32  8092 14640 15210     0]
        [    0     0     0     0     0     0     0     0 15362     0     0     0     0]
        [    0     0     0     0     0     0     0     0     0 15362     0     0     0]
        [    0     0     0     0     0     0     0     0     0     0 15362     0     0]
        [    0     0     0     0     0     0     0     0     0     0     0 15362     0]
        [    0     0     0     0     0     0     0     0  9954  9336 11214  3508    -1]

        sage: pp = PreprocParams(q=7681, fold=1, alpha=4, beta=3, even=True)
        sage: lattice_basis((W, c), pp, theta=1)
        [   2    0    0    0    0    0    0    0 5761 6242 2766 1617    0]
        [   0    1    0    0    0    0    0    0   16   76  361 3635    0]
        [   0    0    2    0    0    0    0    0 5761 1617 4915 6242    0]
        [   0    0    0    1    0    0    0    0   16 3635 7320   76    0]
        [   0    0    0    0    2    0    0    0 5761 1439 2766 6064    0]
        [   0    0    0    0    0    1    0    0   16 7605  361 4046    0]
        [   0    0    0    0    0    0    2    0 5761 6064 4915 1439    0]
        [   0    0    0    0    0    0    0    1   16 4046 7320 7605    0]
        [   0    0    0    0    0    0    0    0 7681    0    0    0    0]
        [   0    0    0    0    0    0    0    0    0 7681    0    0    0]
        [   0    0    0    0    0    0    0    0    0    0 7681    0    0]
        [   0    0    0    0    0    0    0    0    0    0    0 7681    0]
        [   0    0    0    0    0    0    0    0 4977 4668 5607 1754   -1]

        sage: lattice_basis((W, c), pp, theta=2)
        [    2     0     0     0     0     0     0     0 11522 12484  5532  3234     0]
        [    0     1     0     0     0     0     0     0    32   152   722  7270     0]
        [    0     0     2     0     0     0     0     0 11522  3234  9830 12484     0]
        [    0     0     0     1     0     0     0     0    32  7270 14640   152     0]
        [    0     0     0     0     2     0     0     0 11522  2878  5532 12128     0]
        [    0     0     0     0     0     1     0     0    32 15210   722  8092     0]
        [    0     0     0     0     0     0     2     0 11522 12128  9830  2878     0]
        [    0     0     0     0     0     0     0     1    32  8092 14640 15210     0]
        [    0     0     0     0     0     0     0     0 15362     0     0     0     0]
        [    0     0     0     0     0     0     0     0     0 15362     0     0     0]
        [    0     0     0     0     0     0     0     0     0     0 15362     0     0]
        [    0     0     0     0     0     0     0     0     0     0     0 15362     0]
        [    0     0     0     0     0     0     0     0  9954  9336 11214  3508    -1]

    """

    m = A.nrows()
    K = A.base_ring()
    q = K.order()
    n = A.ncols()
    d = m + n*params.b + 1
    num = QQ(theta).numerator()
    den = QQ(theta).denominator()

    L = matrix(ZZ, d, d)

    b = params.b

    for i in range(n):
        for j in range(b):
            row = 2**(j*params.ell) * A.T[i]
            L[i*b+j, i*b+j] = den
            for k in range(m):
                L[i*b+j, n*b + k] = num*ZZ(row[k])

    for i in range(m):
        L[n*b + i, n*b + i] = num*q
        L[-1, n*b + i] = num*ZZ(c[i])

    L[-1, -1] = -1

    if params.even:
        for i in range(0, 2*n, 2):
            for j in range(3*n):
                L[i, j] = (2*L[i, j]) % (num*q)

    return L


def bitflip_norm_expectation(ip, pp, theta=1):
    """
    Return expectation of squared norm of Δ^{(ℓ)}

    :param ip: instance parameters
    :param pp: processing parameters
    :param theta: lattice scaling factor

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=256, q=7681, eta=4, kappa=19)
        sage: bitflip_norm_expectation(ip, PreprocParams(q=7681, fold=3, alpha=6, beta=1, even=False), theta=1)
        2536
        sage: bitflip_norm_expectation(ip, PreprocParams(q=7681, fold=3, alpha=6, beta=1, even=False), theta=2)
        2920

    """
    ell = ZZ(pp.ell)
    beta = ZZ(pp.beta)

    if log(ip.q, 2) == pp.ell * pp.b:
        lhs = (ell - beta)/ell * ip.kappa * ((4**(ell - beta) - 1)/RR(ell - beta)/RR(3))
    else:
        lhs = (ell - beta)/ell * ip.kappa/2. * ((4**(ell - beta) - 1)/RR(ell - beta)/RR(3))
        lhs += max((ell - beta)/ell * ip.kappa/2. * ((4**(ell - beta - 1) - 1)/RR(ell - beta - 1)/RR(3)), 0)
    rhs = ip.n//2**pp.fold * theta**2 * ip.sd**2
    return ceil(lhs + rhs)


@cached_function
def bitflip_norm_estimate(ip, pp, theta=1, ntrials=1024, return_all=False):
    """
    Return median of squared norm of Δ^{(ℓ)}

    :param ip: instance parameters
    :param pp: processing parameters
    :param theta: lattice scaling factor
    :param ntrials: sample
    :param return_all: return all samples instead of the median

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=256, q=7681, eta=4, kappa=19)
        sage: pp = PreprocParams(q=7681, fold=3, alpha=6, beta=1, even=False)
        sage: bitflip_norm_estimate(ip, pp, ntrials=512, theta=1)
        3588
        sage: bitflip_norm_estimate(ip, pp, ntrials=512, theta=2)
        4119
        sage: pp = PreprocParams(q=7681, fold=3, alpha=6, beta=1, even=True)
        sage: bitflip_norm_estimate(ip, pp, ntrials=512, theta=1)
        3761
        sage: bitflip_norm_estimate(ip, pp, ntrials=512, theta=2)
        4158

    """
    t = []

    q = ip.q
    n = ZZ(ip.n//2**pp.fold)

    for _ in range(ntrials):
        d = bitflipf(ip.n, ip.q, ip.kappa)

        if pp.shave_first and pp.alpha:
            d, _ = shave(d, params=pp)

        for _ in range(pp.fold):
            d = balance(d[:len(d)/2] + d[len(d)/2:], q)

        if not pp.shave_first and pp.alpha:
            d, _ = shave(d, params=pp)

        d = snort(d, ip.q, pp.ell, b=pp.b)
        t.append(d.norm()**2 + n * (theta*ip.sd)**2 + 1)

    if return_all:
        return tuple(t)
    else:
        return ceil(median(t))


def enumeration_parameters(B, ip, pp, theta=1):
    from fpylll import IntegerMatrix, GSO, Pruning
    from fpylll.fplll.pruner import svp_probability

    B = IntegerMatrix.from_matrix(B)
    M = GSO.Mat(B)
    M.update_gso()

    t = bitflip_norm_estimate(ip, pp, theta=theta)
    # HACK: block size 60 is somewhat arbitrary, for small lattices we pick something smaller
    bs = min(60, ceil(M.d*0.75))
    # there no point enumerating beyond the length of the shortest vector in the projected sublattice
    target_norm = min(float(bs)/float(M.d) * t, M.get_r(M.d-bs, M.d-bs))
    pruner = Pruning.Pruner(target_norm, 2**40, [M.r(M.d-bs)], 0.9, flags=Pruning.ZEALOUS|Pruning.CVP)
    coefficients = pruner.optimize_coefficients([1. for _ in range(bs)])
    probability = svp_probability(coefficients)
    cost = pruner.single_enum_cost(coefficients)

    return target_norm, coefficients, cost, probability


def predict_attack((d, s), B, ip, pp, theta=1, verbose=False):
    """
    We need to satisfy two conditions (assuming ``even=True``):

    1. Our target ``t`` must be shorter than the shortest vector in the basis
    2. Our target ``t`` must be shorter than (-2^ℓ, 1)

    .. note:: All norms are squared

    """
    shave_cost_ = shave_cost(ip, pp)

    q = ip.q
    ell = pp.ell

    num = QQ(theta).numerator()
    den = QQ(theta).denominator()
    b = pp.b  # number of components in base 2^ℓ
    ell = pp.ell

    actual = snort(d, q, ell=ell, b=b).norm()**2 + (num*s).norm()**2 + 1

    b1 = B[0].norm()**2
    b2 = den**2 * (2**(2*ell) + 1)
    # b3 = (den*vector(ZZ(q).digits(2**ell))).norm()**2
    t = bitflip_norm_estimate(ip, pp, theta=theta)

    b = min([b1, b2])

    target_norm, coefficients, enum_cost, enum_prob = enumeration_parameters(B, ip, pp, theta=theta)

    if verbose:
        fmt =  u"θ: %d, α: %d, β: %d "
        fmt += u"| |Δ|: %7.1f, B[0]: %7.1f, 2^ℓ+1: %7.1f, |Δ|/B[0]: %7.3f, |this|: %7.1f, "
        fmt += u"#enum: 2^%.1f, Pr: %5.1f%%, shave_cost: %5.1f" # noqa
        print fmt%(theta, pp.alpha, pp.beta, t, b1, b2, t/b, actual,
                   log(enum_cost, 2), 100*enum_prob, log(shave_cost_, 2))

    return target_norm, coefficients


"Lattice Reduction"


def progressive_lattice_reduction(B, max_block_size=90, verbose=True, fn=None):
    """
    Run progressive-BKZ with blocksizes increasing by five in each iteration.

    :param B: lattice basis
    :param max_block_size: maximum block size to consider
    :param verbose: print progress information
    :param fn: save the (intermediate) reduced basis to this file

    """
    from fpylll import IntegerMatrix
    from fpylll.algorithms.bkz2 import BKZReduction as BKZ2

    B = IntegerMatrix.from_matrix(B)
    d = min(B.nrows, max_block_size)
    bkz = BKZ2(B)

    block_sizes = tuple(range(20, d, 5)) + (d,)

    for block_size in block_sizes:
        bkz(BKZ.Param(block_size=block_size, strategies=BKZ.DEFAULT_STRATEGY,
                      flags=(BKZ.VERBOSE if verbose else 0)|BKZ.AUTO_ABORT))
        if fn:
            B.to_matrix(matrix(ZZ, B.nrows, B.ncols)).save(fn)
    return B.to_matrix(matrix(ZZ, B.nrows, B.ncols))


def offset_vector(A, t, target_norm, pruning_coefficients):
    """
    Compute the (hopefully shortest) vector Λ(A)-t

    :param A:
    :param t:
    :param target_norm:
    :param pruning_coefficients:

    """

    from fpylll import IntegerMatrix, GSO, Enumeration, EnumerationError

    A = IntegerMatrix.from_matrix(A)
    M = GSO.Mat(A)
    M.update_gso()

    tt = M.from_canonical(t)

    try:
        block_size = len(pruning_coefficients)
        i = M.d-block_size
        enum = Enumeration(M)
        enum_sol = enum.enumerate(i, M.d,
                                  target_norm, 1,
                                  target=tt,
                                  pruning=pruning_coefficients)
        enum_sol = map(int, enum_sol[0][1])
        # take out what we got so far
        t -= vector(A.multiply_left(enum_sol, start=i))

        # handle the rest using Babai's nearest plane
        babai_sol = M.babai(t, dimension=M.d-i)
        return vector(A.multiply_left(babai_sol)) - t
    except EnumerationError:
        return t


def parse_short_vector(v, ip, pp, theta=1):
    """
    Read off the solution from the short vector

    :param v:
    :param ip:
    :param pp:
    :param theta:

    """
    num = QQ(theta).numerator()
    den = QQ(theta).denominator()

    sv = vector(ZZ, v)
    ell = pp.ell
    b = pp.b
    n = ip.n//2**pp.fold
    s = vector(ZZ, [-1*sv[i]//den for i in range(b*n)])
    e = vector(ZZ, [sv[i+b*n]//num for i in range(n)])

    s = sneeze(s, ip.q, ell=ell, b=b)
    return (s, e)


"Kyber instances"


def _kyber_lattice_basis((n, q, theta, even)):
    """
    Construct and save a reduced lattice basis for an NTT instance derived from Kyber parameters.
    """

    if q == 12289:
        N = 1024
    else:
        N = 256

    fold = log(N/n, 2)
    ip = InstanceParams(n=N, q=q, eta=4, kappa=20)
    pp = PreprocParams(q=q, b=2, fold=fold, alpha=0, beta=0, even=even)

    pk, sk = challenge(ip, pp, seed=0)
    W, c = pk
    B = lattice_basis((W, c), pp, theta=theta)
    B, _ = B.submatrix(0, 0, B.nrows()-1, B.ncols()-1), B[-1][:-1]  # noqa

    if q == 12289:
        B.save("basesnh/%s/B-%02d-%d.sobj"%("even" if even else "odd", n, theta))
    else:
        B.save("bases/%s/B-%02d-%d.sobj"%("even" if even else "odd", n, theta))

    # HACK We're removing the unusually short vector
    B = B.submatrix(2, 2, B.nrows()-2, B.ncols()-2)

    if q == 12289:
        progressive_lattice_reduction(B, fn="basesnh/%s/R-%02d-%s-fixed.sobj"%("even" if even else "odd", n, theta))
    else:
        progressive_lattice_reduction(B, fn="bases/%s/R-%02d-%s-fixed.sobj"%("even" if even else "odd", n, theta))
    return True


def kyber_lattices_bases(N=(8, 16, 32), THETA=(1, 2, 3, 4, 5, 6, 7, 8), q=7681, even=False, workers=1):
    """
    Construct and save reduced lattices bases for NTT instances derived from Kyber parameters.
    """
    from multiprocessing import Pool
    pool = Pool(workers)
    jobs = [(n, q, theta, even) for n in N for theta in THETA]
    list(pool.imap_unordered(_kyber_lattice_basis, jobs))


def kyber_run_attack(ip, pp, seed=None, theta=1, verbose=False, pubk=None, seck=None):
    """
    Run attack for one challenge

    :param ip: instance parameter
    :param pp: preprocessing parameter
    :param seed: seed for choosing instance
    :param theta: lattice scaling factor
    :param verbose: print characteristics of the attack
    :returns: ratio between the squared norm of shortest vector found and squared norm of the target vector

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: ip = InstanceParams(n=256, q=7681, eta=4, kappa=10)
        sage: pp = PreprocParams(q=7681, fold=3, alpha=4, beta=1, even=False)
        sage: kyber_run_attack(ip, pp, seed=1, theta=3)[2]
        1.00000000000000

    """
    if pubk is None and seck is None:
        pk, sk = challenge(ip, pp, seed=seed)
        W, c = pk
        d, s = sk
        n_ = ip.n//2**pp.fold
    else:
        W, c = pubk
        d, s = seck
        n_ = W.dimensions()[0]

    # HACK we set Δ_0 to zero
    c -= W.column(0) * d[0]
    d[0] = 0

    # assert(W*d + s == c)

    B = lattice_basis((W, c), pp, theta=theta)  # We only use this to get t
    B, t = B.submatrix(0, 0, B.nrows()-1, B.ncols()-1), B[-1][:-1]
    if ip.q == 12289:   # newhope bases
        B = load("basesnh/%s/R-%02d-%d-fixed.sobj"%("even" if pp.even else "odd", n_, theta))
    else:   # kyber bases
        B = load("bases/%s/R-%02d-%d-fixed.sobj"%("even" if pp.even else "odd", n_, theta))
    t = t[2:]  # We dropped Δ_0

    norm, c = predict_attack((d, s), B, ip, pp, theta=theta, verbose=verbose)
    # print "d", d
    # print "snort(d)", snort(d, ip.q, ell=pp.ell, b=pp.b)
    # print "s", s

    # the actual attack
    b = offset_vector(B, t, norm, c)
    d_, s_ = parse_short_vector([0, 0] + list(b), ip, pp, theta=theta)

    # print "d_", d_
    # print "snort(d_)", snort(d_, ip.q, ell=pp.ell, b=pp.b)
    # print "s_", s_

    return d_, s_, RR(s_.norm()**2/s.norm()**2)


def kyber_lattice_challenge(ip, pp, seed=None, theta=1):
    """
    Return lattices/vectors used in attack.

    :param ip: instance parameter
    :param pp: preprocessing parameter
    :param seed: seed for choosing instance
    :param theta: lattice scaling factor
    :param verbose: print characteristics of the attack
    """
    pk, sk = challenge(ip, pp, seed=seed)

    W, c = pk
    d, s = sk

    # HACK we set Δ_0 to zero
    c -= W.column(0) * d[0]
    d[0] = 0

    assert(W*d + s == c)

    n_ = ip.n//2**pp.fold

    B = lattice_basis((W, c), pp, theta=theta)  # We only use this to get t
    B, t = B.submatrix(0, 0, B.nrows()-1, B.ncols()-1), B[-1][:-1]
    B = load("bases/%s/R-%02d-%d-fixed.sobj"%("even" if pp.even else "odd", n_, theta))
    t = t[2:]  # We dropped Δ_0
    ds = vector(tuple(snort(d, ip.q, ell=pp.ell, b=pp.b)) + tuple(theta*s))

    return B, t, ds[2:]


def kyber_test_attack((theta, alpha, beta), ntrials=8, kappa=19, newhope=False, verbose=True, **kwds):
    """
    Test attack performance.

    EXAMPLE::

        sage: load("poc-main-attack.py")
        sage: set_random_seed(1337)
        sage: kyber_test_attack((3, 4, 2))
        θ: 3, α: 4, β: 2 | |Δ|:  4328.0, B[0]:  7528.0, 2^ℓ+1: 16385.0, |Δ|/B[0]:   ...
        (0.75, 173451777, 19251)

    :param theta, alpha, beta: parameters of the attack
    :param ntrials: number of experiments to run
    :param fold: level of folding
    :param kappa: number of bit flips
    :param newhope: use newhope parameters
    :param verbose: print more intermediate information
    :param even: use even variant of the attack

    """
    if newhope:
        n, q, eta, fold = 1024, 12289, 16, 5
    else:
        n, q, eta, fold = 256, 7681, 4, 3

    ip = InstanceParams(n=n, q=q, eta=eta, kappa=kappa)
    pp = PreprocParams(q=q, alpha=alpha, beta=beta, fold=fold, **kwds)

    assert(kappa > alpha)

    if newhope:
        B = load("basesnh/%s/R-%02d-%d-fixed.sobj"%("even" if pp.even else "odd", ip.n//2**pp.fold, theta))
    else:
        B = load("bases/%s/R-%02d-%d-fixed.sobj"%("even" if pp.even else "odd", ip.n//2**pp.fold, theta))
    target_norm, coefficients, enum_cost, enum_prob = enumeration_parameters(B, ip, pp, theta=theta)

    count = 0

    for i in range(ntrials):
        r = kyber_run_attack(ip, pp, seed=i, verbose=(i==0 if verbose else False), theta=theta)[2]
        count += int(r == 1)
    return float(count)/ntrials, shave_cost(ip, pp), ceil(enum_cost)
