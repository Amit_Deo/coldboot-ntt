# coldboot-ntt

Proof of concept code for the attacks introduced in https://eprint.iacr.org/2018/672.pdf (Cold boot attacks on ring and module LWE keys under the NTT, Martin R. Albrecht, Amit Deo & Kenneth G. Paterson). 