'''
Functions that work a low dimensional solution upwards

Verification of folding correctness:
sage: instp = InstanceParams(n=32, q=7681, sd=sqrt(2), k=2)
sage: pk, (d,s) = instance(instp, seed=2)
sage: (Wp,cp) = foldp(pk,1); (Wm,cm) = foldm(pk,1)
sage: dp,sp = foldp_ds(d,s); dm,sm = foldm_ds(d,s)
sage: Wp*dp+sp==cp; Wm*dm+sm==cm
True
True
sage: Wpp,cpp = foldp((Wp,cp),2); Wpm,cpm = foldm((Wp,cp),2)
sage: dpp,spp = foldp_ds(dp,sp); dpm,spm = foldm_ds(dp,sp)
sage: Wpp*dpp+spp==cpp; Wpm*dpm+spm==cpm
True
True
sage: Wppp,cppp = foldp((Wpp,cpp),3); Wppm,cppm = foldm((Wpp,cpp),3)
sage: dppp,sppp = foldp_ds(dpp,spp); dppm, sppm = foldm_ds(dpp,spp)
sage: Wppp*dppp+sppp==cppp; Wppm*dppm+sppm==cppm
True
True


'''

import poc, bsdr
from sage.all import vector, ZZ, GF, ceil
import time


def verify_solution(pk, s_candidate, err=4):
    ''' pk should be a tuple (A,c) and s_candidate. err is the parameter
        in the binomial distribution used to sample secret i.e. 4 for
        kyber
    '''
    (A, c) = pk
    q = A.base_ring().order()
    err_vec = (c-A*s_candidate)
    for e_ in err_vec:
        if abs(ZZ(e_)) > err and abs(ZZ(e_)) < q-err:
            return False
    return True


def guess_from_partition(pkm, dp, x, q=7681, err=4):
    ''' search through the guesses of sl-sr from the value of sl+sr that have
        x bits set in sl and k-x set in sr (and vice versa)
    '''
    (A, c) = pkm
    bsdr_list = bsdr_from_vec(dp,ceil(log(q,2)))    #this is a list of bsdrs

    for bin_dp in bsdr_list:
        set_positions = bin_dp.nonzero_positions()
        hamming = len(set_positions)
        possibilities = SetPartitions(set_positions, [hamming-x, x])

        for configuration in possibilities:
            for i in range(2):
                if x==0 and i==1:
                    bin_current = flip_signs(bin_dp, [])     #awkward edge case
                else:
                    bin_current = flip_signs(bin_dp, configuration[i])
                current_d = sneeze(bin_current, q, ell=1)
                if verify_solution(pkm, current_d, err) == True:
                    s = balance(c-A*current_d)
                    return (current_d,s)
    return None


def flip_signs(bin_s, positions):
    '''
    flip the signs at the indices in the positions list

    :param bin_s: a binary input vector
    :param positions: a list of indices
    '''
    s = copy(bin_s)
    for i in positions:
        s[i] = -1*s[i]
    return s


def guess_from_sibling(pkm, dp, q=7681, err=4):
    '''
    we have a solution for sl+sr. Need one for sl-sr. So we guess which
    bits of sl+sr come from sl and which come from sr. In other words,
    we guess the bits that need their sign flipping to move from a
    solution of sl+sr to sl-sr. pk is the instance of sl-sr. Note that pkm and
    dp can be replaced by pkp and dm and the function still works.


    :param pkm: the negative fold pk
    :param dp: the solution to dp=dl+dr
    :param q: modulus
    :param err: the bound on the secret (eta)

    '''
    bsdr_list = bsdr_from_vec(dp,ceil(log(q,2)))    #a list of appropriate bsdrs
    num_bit_flips = bsdr_list[0].hamming_weight()

    for x in range(num_bit_flips//2,-1,-1):
        sol = guess_from_partition(pkm, dp, x, q, err)
        if sol != None:
            (d, s) = sol
            return (d, s)
    print "No solution found"
    print "Probable reason: delta vector not sparse enough. Try larger dimension"
    return None


def parent_from_children(dp, dm):
    '''
    Derive parent delta from dp=dl+dr and dm=dl-dr

    :param dp: delta_l+delta_r
    :param dm: delta_l-delta_r
    '''
    dl = (dp + dm)/2
    dr = (dp - dm)/2
    return vector(ZZ,list(dl)+list(dr))


def derive_parent_solution(pkm,dp,q=7681,err=4):
    '''
    Get the parent solution from the positive child solution and instance for
    the negative branch. dp is the actual solution to dp=dl+dr

    :param pkm: the negative fold pk
    :param dp: the solution dp=dl+dr
    :param q: modulus
    :param err: the bound on the secret (eta)

    EXAMPLE::

        sage: attach("poc.py"); attach("bsdr.py"); attach("work_upwards.py")
        sage: instp = InstanceParams(n=32, q=7681, sd=sqrt(2), k=4)
        sage: pk, (d,s) = instance(instp, seed=1)
        sage: dp = foldp_ds(d,s)[0]
        sage: derive_parent_solution(pkm,dp)==d
        Time taken:  0.0513730049133 seconds
        True
    '''
    start_time = time.time()
    try:
        dm, e = guess_from_sibling(pkm,dp,q,err)
    except:
        return None
    print "Time taken: ", time.time() - start_time, "seconds"
    return parent_from_children(dp,dm)


def extract_nq(A):
    '''
    Return n and q from a matrix A

    :param A: a matrix with dimensions nxn and base_ring GF(q)
    '''
    return A.dimensions()[0], A[0][0].base_ring().order()

def original_og(n, q, fold_lvl):
    ''' return the original omega, gamma for the top level instance

    :param n: the current n at the fold_lvl
    :param q: the modulus
    :param fold_lvl: the current fold_lvl

    EXAMPLE::

        sage: n=256; q=7681;
        sage: original_omega=GF(q)(1).nth_root(n)
        sage: original_gamma=original_omega.nth_root(2)
        sage: original_og(128,7681,1) == (original_omega,original_gamma)
        True
        sage: original_og(64,7681,2) == (original_omega,original_gamma)
        True
    '''
    K = GF(q)
    if q == 7681 and n <= 256:
        #then use Kyber omega and gamma
        kyber_omega = K(3844)
        kyber_gamma = K(62)
        original_n = n*2**(fold_lvl)
        power = log(256/original_n,2)
        original_omega = kyber_omega**(2**power)
        original_gamma = kyber_gamma**(2**power)
    else:
        original_n = n*2**(fold_lvl)
        original_omega = K(1).nth_root(original_n)
        original_gamma = original_omega.nth_root(2)
    return original_omega, original_gamma

def current_og(n,q,fold_lvl):
    ''' the omega and gamma to use to derive matrices at fold_lvl

    :param n: the current n at the fold_lvl
    :param q: the modulus
    :param fold_lvl: the current fold_lvl

    EXAMPLE::

        sage: n=256; q=7681;
        sage: original_omega=GF(q)(3844)    #kyber params
        sage: original_gamma=GF(q)(62)  #kyber params
        sage: current_og(128,7681,1) == (original_omega, original_gamma)
        True
        sage: current_og(64,7681,2) == (original_omega^2, original_gamma^2)
        True
    '''
    K=GF(q)
    original_omega, original_gamma = original_og(n,q,fold_lvl)
    omega = K(original_omega)**(2**(fold_lvl-1))
    gamma = K(original_gamma)**(2**(fold_lvl-1))
    return omega, gamma


def Omega_matrix(n, q, omega, gamma):
    '''
    Returns the Omega matrix at fold_lvl and dimension n

    :param n: output dimension of the matrix
    :param q: the modulus
    :param omega: the omega used to create the matrix
    :param gamma: the gamma used to create the matrix
    '''
    K = GF(q)
    Omega = diagonal_matrix(K, [(gamma*omega**i)**(-1) for i in range(n)])
    return Omega

def foldp(pk, fold_lvl):
    '''
    Get the positive fold public key of a parent public key 'pk'

    :param pk: the parent
    :param fold_lvl: the fold level of the child

    EXAMPLE::

        sage: instp = InstanceParams(n=32, q=7681, sd=sqrt(2), k=2)
        sage: pk, (d,s) = instance(instp, seed=2)
        sage: (Wp,cp) = foldp(pk,1); dp,sp = foldp_ds(d,s)
        sage: Wp*dp+sp==cp
        True
        sage: (Wpp,cpp) = foldp((Wp,cp),2); dpp,spp = foldp_ds(dp,sp)
        sage: Wpp*dpp+spp==cpp
        True
    '''
    (W, c) = pk
    (n, q) = extract_nq(W)
    ce = c[0:n:2]
    omega, gamma = current_og(n//2,q,fold_lvl)
    Ap = GF(q)(2)**(-1*fold_lvl)*intt_matrix(n//2,q,omega**2,gamma**2)
    return (Ap,ce)

def foldm(pk, fold_lvl):
    '''
    Get the negative fold public key of a parent positive branch public key 'pk'

    :param pk: the parent public key
    :param fold_lvl: the fold level of the child

    EXAMPLE:

        sage: instp = InstanceParams(n=32, q=7681, sd=sqrt(2), k=2)
        sage: pk, (d,s) = instance(instp, seed=2)
        sage: (Wm,cm) = foldm(pk,1); dm,sm = foldm_ds(d,s)
        sage: Wm*dm+sm==cm
        True
    '''
    (A, c) = pk
    (n, q) = extract_nq(A)
    co = c[1:n:2]
    omega, gamma = current_og(n//2,q,fold_lvl)
    Ap = GF(q)(2)**(-1*fold_lvl)*intt_matrix(n//2,q,omega**2,gamma**2)
    Omega = Omega_matrix(n//2,q,omega,gamma)
    return (Ap*Omega, co)

def foldp_ds(d,s):
    '''
    Fold the secrets d and s down a positive branch

    :param d: the input delta
    :param s: the input s
    '''
    n = len(d)
    return vector(ZZ,d[:n//2]+d[n//2:]), vector(ZZ,s[0:n:2])
def foldm_ds(d,s):
    '''
    Fold the secrets d and s down a negative branch

    :param d: the input delta
    :param s: the input s
    '''
    n = len(d)
    return vector(ZZ,d[:n//2]-d[n//2:]), vector(ZZ,s[1:n:2])
def foldp_multiple_levels(d,s,levels):
    '''
    Fold d and s multiple times down the positive branch

    :param d: input delta
    :param s: input s
    :param levels: number of folds down the positive branch
    '''
    current_d = d
    current_s = s
    next_d = d
    next_s = s
    for i in range(levels):
        next_d,next_s = foldp_ds(current_d,current_s)
        current_d,current_s = next_d, next_s
    return next_d, next_s


def derive_all_negative_instances(pk, folds):
    '''
    Derive all the public keys on the negative leaves so that we can work the
    solution all the way up to the top

    :param pk: initial top level public key
    :param folds: number of folds to perform
    '''
    n, q = extract_nq(pk[0])
    pk_list = []
    current_pk = pk

    for fold_lvl in range(1,folds+1):
        pkp = foldp(current_pk,fold_lvl)
        pkm = foldm(current_pk, fold_lvl)
        pk_list.append(pkm)
        current_pk = pkp

    return pk_list

def derive_bottom_level_instances(pk, folds):
    n, q = extract_nq(pk[0])
    current_pk = pk
    for fold_lvl in range(1,folds+1):
        pkp = foldp(current_pk,fold_lvl)
        if fold_lvl == folds:
            pkm = foldm(current_pk,fold_lvl)
        current_pk = pkp
    return pkp, pkm

def solve_bot_to_top(pk, bot_sol, folds, q=7681, err=4):
    ''' Takes input the original pk=(A,c) and the solution to the bottom positive
        instance.
        bot_sol is the solution to the bottom level positive instance ('dl+dr')

        EXAMPLE::

            sage: attach("poc.py"); attach("bsdr.py"); attach("work_upwards.py")
            sage: instp = InstanceParams(n=32, q=7681, sd=sqrt(2), k=4)
            sage: pk, (d,s) = instance(instp, seed=1)
            sage: dppp = foldp_multiple_levels(d,s,3)[0]  #the solution at the bottom level
            sage: solve_bot_to_top(pk,dppp,folds=3) == d
            Time taken:  0.0245699882507 seconds
            Time taken:  0.0417010784149 seconds
            Time taken:  0.0345211029053 seconds
            True

    '''
    #list of negative instances
    pk_list = derive_all_negative_instances(pk,folds)
    pk_list.reverse()
    current_sol = bot_sol

    #starting from the bottom level instance, work it upwards
    for pks in pk_list:
        current_sol = derive_parent_solution(pks, current_sol, q, err)
        if current_sol == None:
            return None
    return current_sol


def solve_full_instance(pk, sk, ip, pp):
    '''
    The entire attack in one function. This will shave first for now

    :param pk: the top level public key
    :param sk: the top level secret key
    :param ip: InstanceParams for initial instance
    :param pp: PreprocParams to use

    EXAMPLE::

        sage: ip = InstanceParams(n=256, q=7681, sd=sqrt(2), kappa=19)
        sage: pp = PreprocParams(q=7681, fold=3, alpha=8, beta=2, even=True)
        sage: pk,sk = instance(ip,seed=5)
        sage: solve_full_instance(pk,sk,ip,pp)
        Time taken:  1.56107521057 seconds
        Time taken:  16.4021480083 seconds
        Time taken:  24.706097126 seconds
        True
    '''
    W, c_ = pk
    d_, s = sk
    n, q = extract_nq(W)
    folds = pp.fold
    alpha = pp.alpha
    beta = pp.beta
    even = pp.even
    b = pp.b
    pp_ = PreprocParams(q=q,alpha=alpha,beta=beta,b=b,shave_first=True,fold=folds,even=even)

    #shave first, d + offset = d_
    d, offset = shave(d_, pp_)
    c = c_ - W*offset

    #derive bottom level instances
    pkp, pkm = derive_bottom_level_instances((W,c),folds)
    dp_parent, sp_parent = foldp_multiple_levels(d,s,folds-1)
    dp, sp = foldp_ds(dp_parent,sp_parent)
    dm, sm = foldm_ds(dp_parent,sp_parent)
    print (bsdr_from_vec(d,13)[0]).hamming_weight(), (bsdr_from_vec(dp,13)[0]).hamming_weight(),(bsdr_from_vec(dm,13)[0]).hamming_weight()
    dh = (bsdr_from_vec(d,13)[0]).hamming_weight()
    dph = (bsdr_from_vec(dp,13)[0]).hamming_weight()
    dmh = (bsdr_from_vec(dm,13)[0]).hamming_weight()

    success_hw1 = 0
    success_hw2 = 0
    success_lattice = 0
    success_attack = 0
    success_attack64 = 0
    if dh == dph:
        success_hw1 = 1    #working upwards should work
        if dh == dmh:
            success_hw2 = 1

    #solve bottom positive fold
    bot_sol = -1*kyber_run_attack(ip=ip,pp=pp_,theta=3,pubk=pkp,seck=(dp,sp))[0]
    if bot_sol == dp or bot_sol == -1*dp:
        success_lattice = 1
    if bot_sol == -1*dp:
        bot_sol = -1*bot_sol

    #work the solution to the top
    dsol = solve_bot_to_top(pk=(W,c), bot_sol=bot_sol,folds=folds, q=q)

    if dsol == d:
        success_attack = 1
        success_attack64 = 1
        return vector([success_lattice, success_hw1, success_hw2, success_attack, success_attack64])

    dsol = solve_bot_to_top(pk=(W,c), bot_sol=dp_parent,folds=folds-1, q=q)

    if dsol == d:
        success_attack64 = 1

    return vector([success_lattice, success_hw1, success_hw2, success_attack, success_attack64])

    #if fail, solve bottom negative fold and work up
    if success != True:
        #derive the better negative fold instance and solve it
        print "Need to work out how to solve negative instances"
        '''
        #solve the better negative fold instance
        bot_sol_m = kyber_run_attack(ip=ip,pp=pp_,theta=3,pubk=pkm,seck=(dm,sm))[0]
        p_sol = derive_parent_solution(bot_sol, bot_sol_m)
        dsol_ = solve_bot_to_top(pk=(W,c), bot_sol=p_sol, folds=folds-1,q=q)
        if dsol_ == d:
            success = True
            return success
        '''

    return False

def attack_success_rate(ip,pp,total):

    success_vec = vector([0,0,0,0,0])
    for i in range(1,total+1):
        pk,sk = instance(ip,seed=i)
        success = solve_full_instance(pk,sk,ip,pp)
        success_vec = success_vec + success

    print "lattice, hw1, hw2, attack, attack64"
    return success_vec[0]/total.n(), success_vec[1]/total.n(), success_vec[2]/total.n(), success_vec[3]/total.n()


def success_rate_workup(folds, instp, ntrials, execute=False):
    '''
    Run many trials of an instance to work out the success probability of being
    able to work a solution upwards. Note that working a solution upwards only
    fails when the minimal bsdr changes -- so this is the condition we check.
    If execute is true, we actually

    :param folds: number of times we fold
    :param instp: instance parameters to investigate
    :param ntrials: the number of trials
    :param execute: whether to actually execute working upwards
    '''

    k = instp.kappa
    success = vector([0,0]) #first says if kappa is preserved after folding
    for i in range(ntrials):
        pk, (d,s) = instance(instp,seed=i)
        dppp, sppp = foldp_multiple_levels(d,s,folds)
        bsdrs = bsdr_from_vec(dppp,13)
        if bsdrs[0].hamming_weight()==k:
            success[0]+=1

        if execute == True:
            #work up solution from bottom level
            sol_d = solve_bot_to_top(pk,dppp,folds,instp.q,instp.eta)
            if sol_d == d:
                success[1]+=1

            if bsdrs[0].hamming_weight()==k and sol_d!=d:
                print "Failed when we didn't expect to"
            if bsdrs[0].hamming_weight()!=k and sol_d==d:
                print "Succeeded When we didn't expect to"
    return (success[0]/ntrials).n(4), (success[1]/ntrials).n(4)

def distribution_of_kappa(folds,instp,ntrials):
    '''
    we now fold and see what the distribution of the bsdr hamming weight is
    '''
    kappa_list = []
    dif_list = []
    for i in range(ntrials):
        d = bitflipf(instp.n,instp.q,instp.kappa)
        s = vector(ZZ,[0]*instp.n)
        original_hw = bsdr_from_vec(d,13)[0].hamming_weight()
        dppp, sppp = foldp_multiple_levels(d,s,folds)
        final_hw = bsdr_from_vec(dppp,13)[0].hamming_weight()
        kappa_list.append(final_hw)
        dif_list.append(final_hw-original_hw)

    return kappa_list, dif_list


def success_table(max_folds, instp, num_of_runs):
    '''
    Creates a table of the success rates for working solution upwards

    :param max_folds: the largest number of folds to consider
    :param instp: the instance parameters to investigate
    :param num_of_runs: the number of trials to average over

    EXAMPLE::

        sage: attach("work_upwards.py")
        sage: attach("poc.py")
        sage: attach("bsdr.py")
        sage: instp = InstanceParams(n=256, q=7681, sd=sqrt(2), k=19)
        sage: success_table(4,instp,1000)
        0  &  256  &  0.998000000000000 \\
        1  &  128  &  0.904000000000000 \\
        2  &  64  &  0.735000000000000 \\
        3  &  32  &  0.483000000000000 \\
        4  &  16  &  0.191000000000000 \\
    '''
    top_lvl_dimension = instp[0]
    for folds in range(max_folds+1):
        current_dimension = top_lvl_dimension/(2**folds)
        print folds, " & ", current_dimension, " & ",  success_rate(folds,instp,num_of_runs), "\\\\"
